#FrozenFist
##VortexOps

This project codenamed **FrozenFist** is being developed by *VortexOps*. This project involves the design of a software to operate state-of-the-art Semi-Autonomous Armored Assault Vehicles or *SAAAVs*. The vehicles have been designed to operated by a human pilot at a remote location or *Automated* using an integrated *AI* system (Artificial Intelligence).

This file will contain information regarding: 

* Planning
* Design
* Project Schedules
* Other related information

For more information, please feel free to reach out to the development team by [sending us an email](franki.micheo@smail.rasmussen.edu).

### Pushed to BitBucket repository - April 11, 2018

### Modified README on BitBucket - 04/19/2018
![Concept Art](https://images.pexels.com/photos/7376/startup-photos.jpg?cs=srgb&dl=agenda-concept-development-7376.jpg&fm=jpg)
